dnl
dnl CLAY : the Chunky Loop Alteration wizardrY
dnl 
dnl Copyright (C) 2011 Soufiane BAGHDADI
dnl 
dnl This is free software; you can redistribute it and/or modify it under
dnl the terms of the GNU General Public License as published by the Free
dnl Software Foundation; either version 2 of the License, or (at your
dnl option) any later version.
dnl 
dnl This software is distributed in the hope that it will be useful, but
dnl WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl General Public License for more details.
dnl 
dnl You should have received a copy of the GNU General Public License along
dnl with software; if not, write to the Free Software Foundation, Inc.,
dnl 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
dnl 
dnl the Chunky Loop Alteration wizardrY:
dnl a scripting system for high-level loop transformations
dnl Written by Soufiane BAGHDADI <soufiane.baghdadi@inria.fr>
dnl 

