/*
 * CLAY : the Chunky Loop Alteration wizardrY
 * 
 * Copyright (C) 2011 Soufiane BAGHDADI
 * 
 * This is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 * 
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with software; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 * 
 * the Chunky Loop Alteration wizardrY:
 * a scripting system for high-level loop transformations
 * Written by Soufiane BAGHDADI <soufiane.baghdadi@inria.fr>
 * 
 */
# include <stdio.h>
# include <stdlib.h>
/**
 * The clay_options_t structure stores the software/library options for all
 * functions.
 */
struct clay_options {
  char *input_file_path ;   /**< Path of the input scop file. */
  char *input_script_path ; /**< Path of the input script file. */
  char *output_file_path;   /**< Name of the output file. */
  FILE *input_file;   /**< Input file. */
  FILE *input_script; /**< Input script file. */
  FILE *output_file;  /**< Output file. */
  int scanner_debug;  /**< Print all the steps the scanner passes by. */
  int parser_debug;   /**< Print all the steps the parser passes by. */
};
typedef struct clay_options  clay_options_t;
typedef struct clay_options *clay_options_p;


clay_options_p clay_options_malloc(void);
clay_options_p clay_options_read (int argc, char * const argv[]);
void clay_options_help(void);
void clay_options_version(void);
