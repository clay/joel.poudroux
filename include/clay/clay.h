/*
 * CLAY : the Chunky Loop Alteration wizardrY
 * 
 * Copyright (C) 2011 Soufiane BAGHDADI
 * 
 * This is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 * 
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with software; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 * 
 * the Chunky Loop Alteration wizardrY:
 * a scripting system for high-level loop transformations
 * Written by Soufiane BAGHDADI <soufiane.baghdadi@inria.fr>
 * 
 */
# include <stdio.h>
# include <stdlib.h>
# include <options.c>

void clay_parse(clay_options_p);
