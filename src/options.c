/*
 * CLAY : the Chunky Loop Alteration wizardrY
 * 
 * Copyright (C) 2011 Soufiane BAGHDADI
 * 
 * This is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 * 
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with software; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 * 
 * the Chunky Loop Alteration wizardrY:
 * a scripting system for high-level loop transformations
 * Written by Soufiane BAGHDADI <soufiane.baghdadi@inria.fr>
 * 
 */

# include <stdio.h>
# include <stdlib.h>
# include <string.h>

/**
 * The clay_options_t structure stores the software/library options for all
 * functions.
 */
struct clay_options {
  char *input_file_path;   /**< Path of the input scop file. */
  char *input_script_path; /**< Path of the input script file. */
  char *output_file_path;  /**< Name of the output scop file. */
  FILE *input_file;        /**< Input file. */
  FILE *input_script;      /**< Input script file. */
  FILE *output_file;       /**< Output file. */
  FILE *scanner_debug;     /**< Print all the steps the scanner passes by. */
  FILE *parser_debug;      /**< Print all the steps the parser passes by. */
  FILE *error_log;         /**<> */
};
typedef struct clay_options  clay_options_t;
typedef struct clay_options *clay_options_p;

/**
 * clay_options_malloc function:
 * This functions allocate the memory space for a clay_options_t structure and
 * fill its fields with the defaults values. It returns a pointer to the
 * allocated clay_options_t structure.
 **
 * - 24/05/2011: first version.
 */
clay_options_p
clay_options_malloc(void) {
  clay_options_p options = NULL;

  /* Memory allocation for the clay_options_t structure. */
  options = (clay_options_p)malloc(sizeof(clay_options_t));
  if (options == NULL) {
    fprintf(stderr, "[clay]ERROR: memory overflow.\n");
    exit(EXIT_FAILURE);
  }

  /* We set the various fields with default values. */
  options->input_file_path = NULL; /* Name of the input file is not set. */
  options->input_script_path = NULL; /* Name of the input script file is not set. */
  options->output_file_path = NULL; /* Name of the output file is not set. */

  options->input_file = NULL;
  options->input_script = NULL;
  options->output_file = NULL;

  options->scanner_debug = 0;    /* Don't dump the steps the scanner had crossed */
  options->parser_debug = 0;    /* Don't dump the steps the parser had crossed */

  return options;
}

void clay_options_help(void) {
  printf("Help!\n");
}

void clay_options_version(void) {
  printf("Clay: the Chuncky Loop Alteration wizardrY.\n");
  printf("      version 0.01\n");
}

clay_options_p
clay_options_read (int argc, char * const argv[]) {
  int i;
  /* clay_options_t structure allocation and initialization. */
  clay_options_p options;
  options = clay_options_malloc();
  if (argc < 2) {
    printf("[clay]ERROR: input scop file missing. use -h for help\n");
    exit(EXIT_FAILURE);
  }
  else {
    options->input_file_path = argv[1];
    options->input_file = fopen(argv[1], "r");
    if (!options->input_file) {
      printf("[clay]ERROR: %s is not a valid file path or it can not be opened.\n", argv[1]);
      exit(EXIT_FAILURE);
    }
  }
  for (i = 2; i < argc; i++) {
    if (!strcmp(argv[i], "-o")) {
      i++;
      if (argv[i]) {
        options->output_file_path = argv[i];
	options->output_file = fopen(argv[i], "w");
	if (!options->output_file) {
	  printf("[clay]ERROR: %s is not a valid file path or it can not be opened.\n", argv[i]);
	  exit(EXIT_FAILURE);
	}
      }
      else {
        printf ("[clay]WARNING: no output file path after -o option.\n");
      }
    }
    else if (!strcmp(argv[i],"-s") || !strcmp(argv[i],"--script")) {
      i++;
      if (argv[i]) {
        options->input_script_path = argv[i];
	options->input_script = fopen(argv[i], "r");
	if (!options->input_script) {
	  printf("[clay]ERROR: %s is not a valid file path or it can not be opened.\n", argv[i]);
	  exit(EXIT_FAILURE);
	}
      }
      else {
        printf ("[clay]WARNING: no input script path after %s option.\n", argv[i - 1]);
      }
    }
    else if (!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help")) {
      clay_options_help();
    }
    else if (!strcmp(argv[i],"-v") || !strcmp(argv[i],"--version")) {
      clay_options_version();
    }
    else if (argv[i][0] == '-') {
      printf("[clay]WARNING: unknown option %s", argv[i]);
    }
    else {
      printf("[clay]ERROR: please type -h or --help for help\n");
      exit(EXIT_FAILURE);
    }
  }
  printf("input: %s\n", options->input_file_path);
  if (!options->input_script) {
    options->input_script = stdin;
  }
  else {
    printf("script: %s\n", options->input_script_path);
  }
  if (!options->output_file) {
    options->output_file = stdout;
  }
  else {
    printf("output: %s\n", options->output_file_path);
  }
  
  return options;
}