/*
 * CLAY : the Chunky Loop Alteration wizardrY
 * 
 * Copyright (C) 2011 Soufiane BAGHDADI
 * 
 * This is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 * 
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with software; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 * 
 * the Chunky Loop Alteration wizardrY:
 * a scripting system for high-level loop transformations
 * Written by Soufiane BAGHDADI <soufiane.baghdadi@inria.fr>
 * 
 */

 /**
  * \file scanner.l
  * \brief Clay's lexical analyzer.
  * \author Soufiane BAGHDADI
  * \date April 16th 2011
  * this file is a lex file used to generate a lexical analyzer for input
  * script files for Clay the Chuncky Loop Alteration wizardrY.
  */
%{
    # include <stdlib.h>
    # include <string.h>
    # include <parser.h>

    /* a variable used for debugging, if it's different than zero the lexical
     * analyzer will print the read tokens in a file named logfile */
    int POLYTRANS_SCANNER_DEBUG = 0;
    /* used to print the error line when we read an unknown token */
    int line_number = 0;
    
    /* FIXME enter clay optins structure here and mention the logfile output
     * and the error file output*/
%}

%x LINECOMMENT
%x FULLCOMMENT

%%
 /* add C/C++ comments type to the script file */
"/*"               { if (POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     BEGIN FULLCOMMENT;
                   }
<FULLCOMMENT>.     { if (POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     } /* Do nothing */
                   }
<FULLCOMMENT>\n    { line_number++;
                   }
<FULLCOMMENT>"*/"  { if (POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     BEGIN INITIAL;   /* Quit any mode */
                   }

"//"               { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     BEGIN LINECOMMENT; /* Enter LINECOMMENT mode */
                   }
<LINECOMMENT>.     {if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     } /* Do nothing */
                   }  
<LINECOMMENT>\n    { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     line_number++;
                     BEGIN INITIAL;     /* Quit any mode */
                   }

 /* The syntactical form of the transformations accepted by Clay */
"reorder"          { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return REORDER;
                   }
"interchange"      { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return INTERCHANGE;
                   }
"split"            { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return SPLIT;
                   }
"fuse"             { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return FUSE;
                   }
"shift"            { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return SHIFT;
                   }
"skew"             { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return SKEW;
                   }
"iss"              { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return ISS;
                   }

[ \t]*             ; /* Skip whitespaces */
\n                 { line_number++;
                   }

 /* FIXME include -/+ */
[0-9]+             { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     yylval.value = atoi(yytext);
                     return INTEGER;
                   }
"S"                { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return STATEMENT;
                   }
"("                { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return LPARENTHESIS;
                   }
")"                { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return RPARENTHESIS;
                   }
"["                { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return LBRACKET;
                   }
"]"                { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return RBRACKET;
                   }
","                { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:%s\n",yytext);
                     }
                     return COMMA;
                   }
<<EOF>>            { if(POLYTRANS_SCANNER_DEBUG) {
                       fprintf(logfile, "lex:<<EOF>>\n",yytext);
                     }
                     return END;
                   }
[A-Za-z]+          { fprintf("[Clay]Error: unknown syntax at line %d: %s\n",
                             line_number, yytext);
                   }
.                  { fprintf("[Clay]Error: unknown syntax at line %d: %s\n",
                             line_number, yytext);
                   }
%%
