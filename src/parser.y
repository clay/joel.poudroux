%{

  # include <math.h>
  # include "clay.h"
  # include <openscop/scop.h>
  # include <openscop/relation.h>
  # include <openscop/statement.h>
  # include <openscop/macros.h>
  # include "parser.h"
  # include <print.h>
  # include <transformations.h>
  
  int yylex(clay_options_p options);
  void yyerror();
  int yywrap(void);


tuple_p clay_parser_stmttobvect(int stmtnum, openscop_scop_p scop) {
  int i, j;
  tuple_p Head = NULL, t = NULL;
  openscop_statement_p s = scop->statement;
  openscop_relation_p m;
  for (i = 1; ((i < stmtnum)&&(s != NULL)); i++) {
    s=s->next;
  }
  if (s != NULL) {
    m = s->scattering;
    for(i = 0; i < m->nb_rows; i = i + 2) {
      if (i == 0) {
        t = (tuple_p)malloc(sizeof(tuple_t));
        Head = t;
      }
      else {
        t->next = (tuple_p)malloc(sizeof(tuple_t));
        t = t->next;
      }
      t->next = NULL;
      t->nbr = m->m[i][m->nb_columns - 1];
    }
  }
  return Head;
}

  extern FILE * yyin;                  /**< File to be read by Lex */

  int CLAY_PARSER_DEBUG = 1;
%}

%union { int value;       /**< An integer value for integers */
       }

%token REORDER INTERCHANGE SPLIT FUSE SHIFT SKEW ISS
%token INTEGER STATEMENT
%token RPARENTHESIS LPARENTHESIS LBRACKET RBRACKET
%token COMMA
%token END

%type<value> INTEGER

%parse-param {openscop_scop_p scop}
%lex-param { clay_options_p options}


%start Input

%%

Input:              { if(CLAY_PARSER_DEBUG)
                        printf("Transformation (rule)\n");}
    Transformation  { if(CLAY_PARSER_DEBUG)
                        printf("Input (rule)\n");}
    Input           {}
  |                 { if(CLAY_PARSER_DEBUG)
                        printf("END (token)\n");
                      //openscop_scop_print(stderr, scop);
                      clay_print_tocloog(stdout, scop);}
    END             { YYACCEPT;}
  ;

Transformation:     { if(CLAY_PARSER_DEBUG)
                        printf("REORDER (token)\n");
                      betavector = NULL;
                      op_order = NULL;}
    REORDER         { if(CLAY_PARSER_DEBUG)
                        printf("Tupleb (rule)\n");}
    Tupleb          { clay_transformation_reorder(&betavector,
                                                   &op_order, scop);}
  |                 { if(CLAY_PARSER_DEBUG)
                        printf("INTERCHANGE (token)\n");}
    INTERCHANGE     { if(CLAY_PARSER_DEBUG)
                        printf("Tupleb (rule)\n");}
    Tupleb          { clay_transformation_interchange(&betavector,
                                                   &op_order, scop);}
  |                 { if(CLAY_PARSER_DEBUG)
                        printf("SPLIT (token)\n");}
    SPLIT           { if(CLAY_PARSER_DEBUG)
                        printf("Tuple (rule)\n");}
    Tuple           { clay_transformation_split(&betavector, scop);}
  |                 { if(CLAY_PARSER_DEBUG)
                        printf("FUSE (token)\n");}
    FUSE            { if(CLAY_PARSER_DEBUG)
                        printf("Tuple (rule)\n");}
    Tuple           { clay_transformation_fuse(&betavector, scop);}
  |                 { if(CLAY_PARSER_DEBUG)
                        printf("SHIFT (token)\n");}
    SHIFT           { if(CLAY_PARSER_DEBUG)
                        printf("Tupleb (rule)\n");}
    Tupleb          {clay_transformation_shift(&betavector,
                                                   &op_order, scop);}
  |                 { if(CLAY_PARSER_DEBUG)
                        printf("ISS (token)\n");}
    ISS             { if(CLAY_PARSER_DEBUG)
                        printf("Tupleb (rule)\n");}
    Tupleb          {clay_transformation_iss(betavector,
                                                   op_order, scop);}
  ;

Tupleb:             { if(CLAY_PARSER_DEBUG)
                        printf("LPARENTHESIS (token)\n");}
    LPARENTHESIS    { if(CLAY_PARSER_DEBUG)
                        printf("Bvectstmt (rule)\n");}
    Bvectstmt       { if(CLAY_PARSER_DEBUG)
                       printf("LBRACKET (token)\n");}
    LBRACKET        { if(CLAY_PARSER_DEBUG)
                        printf("INTEGER (token)\n");}
    INTEGER         { if(CLAY_PARSER_DEBUG)
                        printf("COMMA (token)\n");
                      templist = (tuple_p)malloc(sizeof(tuple_t));
                      op_order = templist;
                      templist_tile = templist;
                      templist_tile->nbr = $8;
                      templist_tile->next = NULL;}
    COMMA           { if(CLAY_PARSER_DEBUG)
                        printf("INTEGER (token)\n");}
    INTEGER         { templist_tile->next = (tuple_p)malloc(sizeof(tuple_t));
                      templist_tile = templist_tile->next;
                      templist_tile->nbr = $12;
                      templist_tile->next = NULL;}
    Commaintlist    { if(CLAY_PARSER_DEBUG)
                        printf("RBRACKET (token)\n");}
    RBRACKET        { if(CLAY_PARSER_DEBUG)
                        printf("RPARENTHESIS (token)\n");}
    RPARENTHESIS    {}
  ;

Tuple:              { if(CLAY_PARSER_DEBUG)
                        printf("LPARENTHESIS (token)\n");}
    LPARENTHESIS    { if(CLAY_PARSER_DEBUG)
                        printf("Betavector (rule)\n");}
    Bvectstmt       { if(CLAY_PARSER_DEBUG)
                        printf("RPARENTHESIS (token)\n");}
    RPARENTHESIS    {}
  ;

Bvectstmtc:         { if(CLAY_PARSER_DEBUG)
                        printf("INTEGER (token)\n");}
    INTEGER         { if(CLAY_PARSER_DEBUG)
                        printf("COMMA (token)\n");
                      templist = (tuple_p)malloc(sizeof(tuple_t));
                      betavector = templist;
                      templist_tile = templist;
                      templist_tile->nbr = $2;
                      templist_tile->next = NULL;}
    COMMA           { if(CLAY_PARSER_DEBUG)
                        printf("Intcommalist (rule)\n");}
    Intcommalist    {}
  |                 { if(CLAY_PARSER_DEBUG)
                        printf("STATEMENT (token)\n");}
    STATEMENT       { if(CLAY_PARSER_DEBUG)
                        printf("INTEGER (token)\n");}
    INTEGER         { if(CLAY_PARSER_DEBUG)
                        printf("COMMA (token)\n");}
    COMMA           { betavector = clay_parser_stmttobvect($4, scop);}
  |
  ;

Bvectstmt:          { if(CLAY_PARSER_DEBUG)
                        printf("INTEGER (token)\n");}
    INTEGER         { if(CLAY_PARSER_DEBUG)
                        printf("Intcommalist (rule)\n");
                      templist = (tuple_p)malloc(sizeof(tuple_t));
                      betavector = templist;
                      templist_tile = templist;
                      templist_tile->nbr = $2;
                      templist_tile->next = NULL;}
    Commaintlist    {}
  |                 { if(CLAY_PARSER_DEBUG)
                        printf("STATEMENT (token)\n");}
    STATEMENT       { if(CLAY_PARSER_DEBUG)
                        printf("INTEGER (token)\n");}
    INTEGER         { betavector = clay_parser_stmttobvect($4, scop);}
  ;

Intcommalist:       { if(CLAY_PARSER_DEBUG)
                        printf("INTEGER (token)\n");}
    INTEGER         { if(CLAY_PARSER_DEBUG)
                        printf("COMMA (token)\n");
                      templist_tile->next = (tuple_p)malloc(sizeof(tuple_t));
                      templist_tile = templist_tile->next;
                      templist_tile->nbr = $2;
                      templist_tile->next = NULL;}
    COMMA           { if(CLAY_PARSER_DEBUG)
                        printf("Intcommalist (rule)\n");}
    Intcommalist    {}
  |                 {}
  ;

Commaintlist:       { if(CLAY_PARSER_DEBUG)
                        printf("COMMA (token)\n");}
    COMMA           { if(CLAY_PARSER_DEBUG)
                        printf("INTEGER (token)\n");}
    INTEGER         { if(CLAY_PARSER_DEBUG)
                        printf("Integerlist (rule)\n");
                      templist_tile->next = (tuple_p)malloc(sizeof(tuple_t));
                      templist_tile = templist_tile->next;
                      templist_tile->nbr = $4;
                      templist_tile->next = NULL;}
    Commaintlist    {}
  |                 {}
  ;

%%

void yyerror(){}
int yywrap() {return 1;}

void
clay_parse(clay_options_p options)
{
  yyin = options->input_script;
  openscop_scop_p scop = openscop_scop_read(options->input_file);
  yyparse(scop);
  fclose(yyin);
}
