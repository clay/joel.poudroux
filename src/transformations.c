/*
 * CLAY : the Chunky Loop Alteration wizardrY
 * 
 * Copyright (C) 2011 Soufiane BAGHDADI
 * 
 * This is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 * 
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with software; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 * 
 * the Chunky Loop Alteration wizardrY:
 * a scripting system for high-level loop transformations
 * Written by Soufiane BAGHDADI <soufiane.baghdadi@inria.fr>
 * 
 */

# include <transformations.h>

void clay_transformation_reorder(tuple_p *betavector, tuple_p *op_order,
                                  openscop_scop_p scop) {
  printf("reorder...\n");
  int i, j;
  openscop_statement_p s = scop->statement;
  openscop_relation_p m;
  tuple_p t;
  for (; s != NULL; s=s->next) {
    for (m = s->scattering; m; m = m->next) {
      t = *betavector;
      for (i = 0; (i < m->nb_rows)&&(t != NULL); i=i+2, t = t->next)
      {
        if (SCOPINT_ne(m->m[i][m->nb_columns - 1],
                                       t->nbr)) {
          break;
        }
      }
      if (t == NULL) {
        t = *op_order;
        for(j = 0; t != NULL; j++, t=t->next) {
          if(SCOPINT_eq(m->m[i][m->nb_columns - 1], t->nbr)) {
            m->m[i][m->nb_columns - 1] = (openscop_int_t)j;
            break;
          }
        }
      }
    }
  }
}

void clay_transformation_interchange(tuple_p *betavector,
                                 tuple_p *op_order, openscop_scop_p scop) {
  printf("interchange...\n");
  openscop_int_t ;
  int i, j;
  openscop_statement_p s = scop->statement;
  openscop_relation_p m;
  tuple_p t;
  for (; s != NULL; s=s->next) {
    m = s->scattering;
    t = *betavector;
    for (i = 0; (i < m->nb_rows)&&(t != NULL); i=i+2, t = t->next) {
      if (m->m[i][m->nb_columns - 1] != t->nbr) {
        break;
      }
    }
    if (t == NULL) {
      t = *op_order;
      openscop_int_t **p = (openscop_int_t **)
        malloc(m->nb_rows*sizeof(openscop_int_t *));
      for(j = 0 ; j < m->nb_rows; j++) {
        p[j] = &(m->m[j][0]);
      }
      for(j = 0; t != NULL; j++, t = t->next) {
        p[2 * j + 1] = &(m->m[2 * t->nbr + 1][0]);
      }
      free(m->m);
      m->m = p;
    }
  }
}

void clay_transformation_fuse(tuple_p *betavector, openscop_scop_p scop) {
  printf("fuse...\n");
  int i, j, max = 0;
  openscop_statement_p s = scop->statement;
  openscop_relation_p m;
  tuple_p t, t2, tobemerged = NULL;
  for (; s != NULL; s=s->next) {
    m = s->scattering;
    t = *betavector;
    if (t != NULL) {
      for (i = 0; (i < (m->nb_rows - 3)) && (t->next != NULL);
                                                   i=i+2, t = t->next) {
        if (m->m[i][m->nb_columns - 1] != t->nbr) {
          break;
        }
      }
      if ((t->next == NULL) || (i >= (m->nb_rows - 3))) {
        if (m->m[i + 2][m->nb_columns - 1] < max) {
          max = m->m[i + 2][m->nb_columns - 1];
        }
      }
    }
  }
  s = scop->statement;
  for (; s != NULL; s=s->next) {
    m = s->scattering;
    t = *betavector;
    if (t != NULL) {
      for (i = 0; (i < (m->nb_rows - 3)) && (t->next != NULL);
                                                   i=i+2, t = t->next) {
        if (m->m[i][m->nb_columns - 1] != t->nbr) {
          break;
        }
      }
      if ((t->next == NULL) || (i >= (m->nb_rows - 3))) {
        if (m->m[i][m->nb_columns - 1] == (t->nbr + 1)) {
          m->m[i + 2][m->nb_columns - 1] += max+1;
          m->m[i][m->nb_columns - 1]--;
        }
      }
    }
  }
}

void clay_transformation_split(tuple_p *betavector, openscop_scop_p scop) {
  printf("split...\n");
  int i, j;
  openscop_statement_p s = scop->statement;
  openscop_relation_p m;
  tuple_p t;
  for (; s != NULL; s=s->next) {
    m = s->scattering;
    t = *betavector;
    if (t != NULL) {
      if (t->next != NULL) {
        for (i = 0; (i < m->nb_rows)&&((t->next)->next != NULL);
             i=i+2, t = t->next) {
          if (m->m[i][m->nb_columns - 1] != t->nbr) {
            break;
          }
        }
        if ((t->next)->next == NULL) {
          if ((m->m[i][m->nb_columns - 1] == t->nbr)  &&
                    (m->m[i + 2][m->nb_columns - 1] >= (t->next)->nbr)) {
            m->m[i + 2][m->nb_columns - 1] -= (t->next)->nbr;
            m->m[i][m->nb_columns - 1] += 1;
          }
          else if (m->m[i][m->nb_columns - 1] > t->nbr) {
            m->m[i][m->nb_columns - 1] += 1;
          }
        }
      }
    }
  }
}

void clay_transformation_shift(tuple_p *betavector, tuple_p *op_order,
                                  openscop_scop_p scop) {
  printf("shift...\n");
  int i, j;
  openscop_statement_p s = scop->statement;
  openscop_relation_p m;
  tuple_p t;
  for (; s != NULL; s=s->next) {
    m = s->scattering;
    t = *betavector;
    for (i = 0; (i < m->nb_rows)&&(t != NULL); i = i + 2, t = t->next)
    {
      if (m->m[i][m->nb_columns - 1] != t->nbr) {
        break;
      }
    }
    if (t == NULL) {
      t = *op_order;
      for(j = 1; t != NULL; j+2, t=t->next) {
        m->m[j][m->nb_columns - 1] += t->nbr;
      }
    }
  }
}

void clay_transformation_strip_mine(tuple_p *betavector,
            int *strip, openscop_scop_p scop) {
  printf("strip_mine...\n");
  int i, j;
  openscop_statement_p s = scop->statement;
  openscop_relation_p m;
  tuple_p t;
  for (; s != NULL; s=s->next) {
    m = s->scattering;
    t = *betavector;
    for (i = 0; (i < m->nb_rows)&&(t != NULL); i = i + 2, t = t->next)
    {
      if (m->m[i][m->nb_columns - 1] != t->nbr) {
        break;
      }
    }
    if (t == NULL) {
      openscop_relation_p new_relation;
      new_relation = openscop_relation_malloc(m->nb_rows, m->nb_columns+2);
      new_relation->nb_parameters  = m->nb_parameters;
      new_relation->nb_output_dims = m->nb_output_dims + 2;
      new_relation->nb_input_dims  = m->nb_input_dims;
      new_relation->nb_local_dims  = m->nb_local_dims;
    }
  }
}

/*redesigned 11/07/2011*/
void clay_transformation_iss(tuple_p betavector, tuple_p op_order,
                                  openscop_scop_p scop) {
  printf("iss...\n");
  int i, j;
  openscop_statement_p s = scop->statement;
  openscop_relation_p r, old_r;
  tuple_p t;
  int relation_found;
  for (; s != NULL; s=s->next) {
    r = s->scattering;
    j = 0;
    t = betavector;
    relation_found = 1;
    for (i = 0; (i < 2 * r->nb_output_dims + 1)&&(t != NULL); t = t->next, i++, j++) {
      for (; r->m[j][0] != 0; ) {
	j++;
      }
      if (r->m[j][r->nb_columns - 1] != t->nbr) {
	relation_found = 0;
        break;
      }
    }
    if (relation_found) {
      old_r = NULL;
      if (t != NULL) {
        for (i = 0; (i < t->nbr)&&(r != NULL); i++) {
	  old_r = r;
	  r = r->next;
        }
        if (r == NULL) {
	  fprintf(stderr, "[Clay]Error: there isn't many unions in statement");
	  for (; betavector != NULL; betavector = betavector->next) {
	    fprintf(stdout, " %d", betavector->nbr);
	  }
	  fprintf(stdout, "\n");
	  exit(EXIT_FAILURE);
        }
      }
      //union part is found, we can apply the transformation
      openscop_relation_p new_r;
      // create the first union part
      new_r = openscop_relation_malloc(r->nb_rows+1, r->nb_columns);
      new_r->type = r->type;
      new_r->nb_output_dims = r->nb_output_dims;
      new_r->nb_input_dims = r->nb_input_dims;
      new_r->nb_local_dims = r->nb_local_dims;
      new_r->nb_parameters = r->nb_parameters;
      for (i = 0; i < r->nb_rows; i++) {
        for (j = 0; j < r->nb_columns; j++) {
          SCOPINT_assign(new_r->m[i][j], r->m[i][j]);
	}
      }
      SCOPINT_assign(new_r->m[new_r->nb_rows-1][0], 1);
      for (i = 1; i <= new_r->nb_output_dims; i++) {
	SCOPINT_assign(new_r->m[new_r->nb_rows-1][i], 0);
      }
      t = op_order;
      for (; t != NULL; i++, t = t->next) {
	SCOPINT_assign(new_r->m[new_r->nb_rows-1][i], t->nbr);
      }
      new_r->next = r->next;
      if (old_r != NULL) {
	old_r->next = new_r;
      }
      else {
	s->scattering = new_r;
      }
      // create the second union part
      old_r = new_r;
      new_r = openscop_relation_malloc(r->nb_rows+1, r->nb_columns);
      new_r->type = r->type;
      new_r->nb_output_dims = r->nb_output_dims;
      new_r->nb_input_dims = r->nb_input_dims;
      new_r->nb_local_dims = r->nb_local_dims;
      new_r->nb_parameters = r->nb_parameters;
      for (i = 0; i < r->nb_rows; i++) {
        for (j = 0; j < r->nb_columns; j++) {
          SCOPINT_assign(new_r->m[i][j], r->m[i][j]);
	}
      }
      SCOPINT_assign(new_r->m[new_r->nb_rows-1][0], 1);
      for (i = 1; i <= new_r->nb_output_dims; i++) {
	SCOPINT_assign(new_r->m[new_r->nb_rows-1][i], 0);
      }
      t = op_order;
      for (; t != NULL; i++, t = t->next) {
	SCOPINT_assign(new_r->m[new_r->nb_rows-1][i], -1*t->nbr);
      }
      if (old_r != NULL) {
	new_r->next = r->next;
	old_r->next = new_r;
      }
      else {
	fprintf(stderr, "[Clay]Error: you shouldn't be here!");
      }
      openscop_relation_free(r);
    }
  }
}

/*
void clay_transformation_ (tuple_p *betavector, tuple_p *op_order,
                                  openscop_scop_p scop) {
  printf("iss...\n");
  int i, j;
  openscop_statement_p s = scop->statement;
  openscop_relation_p r, old_r;
  tuple_p t;
  for (; s != NULL; s=s->next) {
    r = s->scattering;
    j = 0;
    t = betavector;
    for (i = 0; (i < 2 * r->nb_output_dims + 1)&&(t != NULL); t = t->next, i++, j++) {
      for (; r->m[j][r->nb_columns - 1] != 0; ) {
	j++;
      }
      if (r->m[j][r->nb_columns - 1] != t->nbr) {
        break;
      }
    }
    if (t != NULL) {
      for (i = 0; (i < t->nbr)&&(r != NULL); i++) {
	r = r->next;
      }
      if (r == NULL) {
	fprintf(stderr, "[Clay]Error: there isn't many unions in statement");
	for (; betavector != NULL; betavector = betavector->next) {
	  fprintf(stdout, " %d", betavector->nbr);
	}
	fprintf(stdout, "\n");
	exit(EXIT_FAILURE);
      }
      //union part is found, we can apply the transformation
    }
  }
}*/